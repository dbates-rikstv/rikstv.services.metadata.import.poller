﻿using System;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Settings
{
    public class PartnerApiSettings
    {
        public string EpisodesBaseUrl { get; set; }
        public string MoviesBaseUrl { get; set; }

        public DateTime? ModifiedSince { get; set; }
    }
}
