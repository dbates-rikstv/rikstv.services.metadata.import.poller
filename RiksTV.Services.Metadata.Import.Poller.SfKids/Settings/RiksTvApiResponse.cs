﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Settings
{
    public enum RiksTvApiResult
    {
        Successful = 0,
        BadRequest = 1,
        AccessDenied = 2,
        NullData = 3,
        Unknown = 4
    }
    public class RiksTvApiResponse
    {
        public bool Successful => Result == RiksTvApiResult.Successful;

        public RiksTvApiResult Result { get; private set; }
        public string ErrorMessage { get; private set; }

        public static RiksTvApiResponse Success()
        {
            return new RiksTvApiResponse
            {
                Result = RiksTvApiResult.Successful
            };
        }

        public static RiksTvApiResponse Failure(RiksTvApiResult result, string message)
        {
            return new RiksTvApiResponse
            {
                Result = result,
                ErrorMessage = message
            };
        }
    }
}
