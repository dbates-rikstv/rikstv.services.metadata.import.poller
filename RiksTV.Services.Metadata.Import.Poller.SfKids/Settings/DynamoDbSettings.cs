﻿namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Settings
{
    public class DynamoDbSettings
    {
        public string AccessKey { get; set; }
        public string Secret { get; set; }
        public string Table { get; set; }
        public int CreateTableTimeout { get; set; }
    }
}