﻿namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Settings
{
    public class RiksTvApiSettings
    {
        public string EndpointUrl { get; set; }
    }
}
