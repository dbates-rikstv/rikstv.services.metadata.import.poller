﻿using System;
using System.Net.Http;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RiksTV.Core.Logging;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Configuration;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Services;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using Serilog;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids
{
    class Program
    {
        private static IConfigurationRoot _configuration;

        static void Main(string[] args)
        {
            var provider = Configure();

            var persistence = provider.GetService<IPersistence>();
            var persistenceSetupResult = persistence.Setup().Result;

            if (persistenceSetupResult.Successful)
            {
                var interval = int.Parse(_configuration["Schedule:IntervalSeconds"]);
                provider.GetService<App>().Run(interval);
                Console.ReadKey();
            }
            else
            {
                Log.Logger.Error($"Failed to start {persistenceSetupResult.ErrorMessage}");
            }
        }

        private static IServiceProvider Configure()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables();
            _configuration = builder.Build();

            var services = new ServiceCollection();
            var provider = ConfigureServices(services);

            Log.Logger = new LoggerConfiguration()
                .ConfigureRiksTvLogging(Assembly.GetExecutingAssembly().GetName().Name, _configuration["Serilog:LogLevel"])
                .CreateLogger();

            return provider;
        }

        private static IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions()
                .Configure<PartnerApiSettings>(_configuration.GetSection("PartnerApi"))
                .Configure<RiksTvApiSettings>(_configuration.GetSection("RiksTvApi"))
                .Configure<DynamoDbSettings>(_configuration.GetSection("DynamoDb"));

            services.AddLogging(loggingBuilder =>
                loggingBuilder.AddSerilog(dispose: true));

            return ServiceProvider(services);
        }

        private static IServiceProvider ServiceProvider(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterType<HttpClient>().SingleInstance();

            builder.RegisterModule<ServicesModule>();

            builder.RegisterType<App>().SingleInstance();
            builder.RegisterLogger();

            builder.RegisterType<DynamoDb>().As<IDynamoDb>().SingleInstance();

            var applicationContainer = builder.Build();

            return new AutofacServiceProvider(applicationContainer);
        }

    }
}
