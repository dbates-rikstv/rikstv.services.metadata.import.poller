﻿using System;
using System.Data.SqlTypes;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence.Models;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Services;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids
{
    public class App
    {
        private readonly ILogger<App> _logger;
        private readonly ISfFetcher _fetcher;
        private readonly IRiksTvPoster _poster;
        private readonly IPersistence _persistence;
        private readonly PartnerApiSettings _partnerApiSettings;

        public App(ILogger<App> logger, ISfFetcher fetcher, IRiksTvPoster poster, IPersistence persistence, IOptions<PartnerApiSettings> partnerApiSettings)
        {
            _logger = logger;
            _fetcher = fetcher;
            _poster = poster;
            _persistence = persistence;
            _partnerApiSettings = partnerApiSettings.Value;
        }

        public void Run(int seconds)
        {
            _logger.LogInformation("Begin processing");
            Observable.Interval(TimeSpan.FromSeconds(seconds)).StartWith(0)
                .Subscribe(x => { ScanPartnerApi().Wait(); });
        }

        private async Task ScanPartnerApi()
        {
            _logger.LogInformation("Start scanning the SF partner API");

            // Set last run time.
            var sfKidsPercistency = await _persistence.Load<SfKidsPersistence>();
            _partnerApiSettings.ModifiedSince = sfKidsPercistency?.ModifiedSince;


            var started = DateTime.Now;

            object asset = null;
            do
            {
                asset = await _fetcher.GetNextChangedAsset();
                if (asset != null)
                {
                    var result = await _poster.PostAsset((JObject)asset);
                    if (result.Successful)
                    {
                        _logger.LogInformation("RiksTv POST successfull");
                    }
                    else
                    {
                        _logger.LogError($"RiksTv POST failed ({result.ErrorMessage})");
                    }
                }

                asset = null;
            } while (asset != null);

            if (sfKidsPercistency == null)
            {
                sfKidsPercistency = new SfKidsPersistence();
            }

            sfKidsPercistency.ModifiedSince = started;
            var persistencyResult = await _persistence.Save(sfKidsPercistency);

            if (!persistencyResult.Successful)
            {
                _logger.LogError($"Persistency SAVE failed ({persistencyResult.ErrorMessage})");
            }
        }
    }
}

