﻿using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using System.Net.Http;
using System.Threading.Tasks;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Services
{
    public class RiksTvPoster : IRiksTvPoster
    {
        private readonly RiksTvApiSettings _settings;
        private readonly HttpClient _httpClient;
        private readonly ILogger<App> _logger;

        public RiksTvPoster(IOptions<RiksTvApiSettings> settings, HttpClient httpClient, ILogger<App> logger)
        {
            _settings = settings.Value;
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<RiksTvApiResponse> PostAsset(JObject asset)
        {
            if (asset != null)
            {
                var response = await _httpClient.PostAsJsonAsync(_settings.EndpointUrl, asset);

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        return RiksTvApiResponse.Success();
                    case HttpStatusCode.BadRequest:
                        return RiksTvApiResponse.Failure(RiksTvApiResult.BadRequest, "Provided data not accepted.");
                    case HttpStatusCode.Forbidden:
                        return RiksTvApiResponse.Failure(RiksTvApiResult.AccessDenied, "Call made from illegal IP address.");
                    default:
                        return RiksTvApiResponse.Failure(RiksTvApiResult.Unknown, "Unknown response from API");
                }
            }
            else
            {
                return RiksTvApiResponse.Failure(RiksTvApiResult.NullData, "Can't post null data object.");
            }
        }
    }
}
