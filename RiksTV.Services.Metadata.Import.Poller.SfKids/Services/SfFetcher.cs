﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Services
{
    public class SfFetcher : ISfFetcher
    {
        private enum PageType
        {
            Episode = 0,
            Movie = 1
        }

        private PageType _pageType;
        private JObject _page;
        private Queue<JToken> _assets;
        private readonly PartnerApiSettings _settings;
        private readonly HttpClient _httpClient;
        private readonly ILogger<App> _logger;

        public SfFetcher(IOptions<PartnerApiSettings> settings, HttpClient httpClient, ILogger<App> logger)
        {
            _settings = settings.Value;
            _httpClient = httpClient;
            _logger = logger;

            _page = null;
        }

        private async Task<JObject> GetAsync(string url)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(url)
            };

            var response = await _httpClient.SendAsync(request).ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return JObject.Parse(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
        }

        private void LoadPage(string url)
        {
            _logger.LogInformation($"Loading data page {url}");
            _page = GetAsync(url).GetAwaiter().GetResult();
            _assets = new Queue<JToken>(_page.GetValue("Items"));
        }

        public async Task<JObject> GetNextChangedAsset()
        {
            if (_page == null)
            {
                LoadPage(BuildUrl(_settings.EpisodesBaseUrl));
                _pageType = PageType.Episode;
            }
            else
            {
                if (_assets.Count == 0)
                {
                    if (_page.ContainsKey("Next"))
                    {
                        LoadPage((string) _page["Next"]["Href"]);
                    }
                    else if (_pageType == PageType.Episode)
                    {
                        LoadPage(BuildUrl(_settings.MoviesBaseUrl));
                        _pageType = PageType.Movie;
                    }
                }
            }

            if (_assets != null && _assets.Count > 0)
            {
                var asset = _assets.Dequeue();

                if (asset != null)
                {
                    var self = GetAsync((string) asset["Self"]["Href"]).GetAwaiter().GetResult();


                    if (_pageType == PageType.Episode && self["Item"]["Season"] == null)
                    {
                        _logger.LogWarning($"Skipping asset {(string)self["Item"]["Id"]}, Missing season information.");
                        return await GetNextChangedAsset();
                    }

                    if (IsSVOD(self))
                    {
                        var returnObject = new JObject();

                        returnObject["type"] = _pageType == PageType.Episode ? "Series" : "Movie";
                        returnObject["item"] = self;

                        if (_pageType == PageType.Episode)
                        {
                            var season = GetAsync((string) self["Item"]["Season"]["Href"]).GetAwaiter().GetResult();
                            var series = GetAsync((string) season["Item"]["Series"]["Href"]).GetAwaiter().GetResult();
                            var episodes = GetAsync((string) season["Item"]["Episodes"]["Href"]).GetAwaiter()
                                .GetResult();

                            returnObject["episodecount"] = episodes["TotalCount"];
                            returnObject["season"] = season;
                            returnObject["series"] = series;
                        }

                        _logger.LogInformation($"Loaded SVOD asset {(string) self["Item"]["Id"]}");

                        return returnObject;
                    }
                    else
                    {
                        _logger.LogWarning($"Skipping asset {(string)self["Item"]["Id"]}, no SVOD business type.");
                        return await GetNextChangedAsset();
                    }
                }
            }

            _page = null; // Reset service.

            return null;
        }

        private bool IsSVOD(JToken self)
        {
            if (self["Item"] != null && self["Item"]["DeviceData"] != null)
            {
                return self["Item"]["DeviceData"].FirstOrDefault(dd => dd != null && ((string) dd["Name"]) == "STB")?
                           ["CountryData"].FirstOrDefault(cd =>
                               cd != null && ((string) cd["Country"]) == "no" &&
                               ((string) cd["BusinessType"]) == "SVOD") != null;
            }

            return false;
        }

        private string BuildUrl(string baseUrl)
        {
            if (_settings.ModifiedSince == null)
                return baseUrl;

            var modifiedSince = ((DateTime)_settings.ModifiedSince).ToString("o");

            return $"{baseUrl}?modifiedSince={modifiedSince}";
        }
    }
}