﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Services
{
    public interface ISfFetcher
    {
        Task<JObject> GetNextChangedAsset();
    }
}
