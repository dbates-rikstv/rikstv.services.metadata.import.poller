﻿using Newtonsoft.Json.Linq;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using System.Threading.Tasks;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Services
{
    public interface IRiksTvPoster
    {
        Task<RiksTvApiResponse> PostAsset(JObject asset);
    }
}
