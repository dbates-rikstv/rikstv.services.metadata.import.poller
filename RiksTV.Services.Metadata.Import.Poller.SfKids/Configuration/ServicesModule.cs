﻿using Autofac;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Services;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Configuration
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SfFetcher>().As<ISfFetcher>();
            builder.RegisterType<RiksTvPoster>().As<IRiksTvPoster>();
            builder.RegisterType<DynamoDbPersistence>().As<IPersistence>();
        }
    }
}
