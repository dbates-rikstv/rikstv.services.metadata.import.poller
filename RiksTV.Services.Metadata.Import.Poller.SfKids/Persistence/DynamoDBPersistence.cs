﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence.Models;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence
{
    public class DynamoDbPersistence : IPersistence
    {
        private readonly IDynamoDb _dynamo;
        private readonly DynamoDbSettings _settings;
        private readonly ILogger<App> _logger;
        private static string _tableName = "RiksTv.Services.Metadata.Import.Poller";

        public DynamoDbPersistence(IDynamoDb dynamo,IOptions<DynamoDbSettings> settings, ILogger<App> logger)
        {
            _dynamo = dynamo;
            _settings = settings.Value;
            _logger = logger;
        }

        public async Task<T> Load<T>()
        {
            return await _dynamo.LoadAsync<T>(typeof(T).Name);
        }


        public async Task<PersistenceResponse> Save(PercistenceModel model)
        {
            if (model != null)
            {
                if (model.GetType() == typeof(SfKidsPersistence))
                {
                    await _dynamo.SaveAsync<SfKidsPersistence>(model);

                    return PersistenceResponse.Success();
                }
            }

            return PersistenceResponse.Failure(PersistenceResult.NotImplemented,"Storage of model type not implemented.");
        }

        public async Task<PersistenceResponse> Setup()
        {
            // Create DynamoDb table if non existing
            try
            {
                var tableDescription = _dynamo.DescribeTableAsync(_tableName).Result;

                if (_dynamo.IsTableActive(_tableName))
                {
                    return PersistenceResponse.Success();
                }
                else
                {
                    return PersistenceResponse.Failure(PersistenceResult.NotReady,"DynamoDB Table not ready.");
                }
            }
            catch
            {
                // Exception on non existing table.
            }

            _logger.LogInformation($"Creating DynamoDb Table {_tableName}");
            var request = new CreateTableRequest
            {
                AttributeDefinitions = new List<AttributeDefinition>()
                {
                    new AttributeDefinition
                    {
                        AttributeName = "Service",
                        AttributeType = "S"
                    }
                },
                KeySchema = new List<KeySchemaElement>
                {
                    new KeySchemaElement
                    {
                        AttributeName = "Service",
                        KeyType = "HASH" //Partition key
                    }
                },
                ProvisionedThroughput = new ProvisionedThroughput
                {
                    ReadCapacityUnits = 2,
                    WriteCapacityUnits = 2
                },
                TableName = _tableName
            };

            var response = await _dynamo.CreateTableAsync(request);
            if (response.HttpStatusCode == HttpStatusCode.OK && response.TableDescription != null)
            {
                // Wait until fully created.
                int seconds = _settings.CreateTableTimeout;
                while (seconds-- > 0)
                {
                    System.Threading.Thread.Sleep(1000);
                    if (_dynamo.IsTableActive(_tableName) == true)
                    {
                        return PersistenceResponse.Success();
                    }
                }

                return PersistenceResponse.Failure(PersistenceResult.NotReady, "DynamoDB Table not ready.");
            }

            return PersistenceResponse.Failure(PersistenceResult.Failed,"Can't create percistency table.");
        }
    }
}
