﻿using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using Microsoft.Extensions.Options;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence
{
    public class DynamoDb : IDynamoDb
    {
        private readonly DynamoDbSettings _settings;

        public DynamoDb(IOptions<DynamoDbSettings> settings)
        {
            _settings = settings.Value;
        }

        public async Task<CreateTableResponse> CreateTableAsync(CreateTableRequest request)
        {
            return await GetClient().CreateTableAsync(request);
        }

        public async Task<DescribeTableResponse> DescribeTableAsync(string table)
        {
            return await GetClient().DescribeTableAsync(table);
        }

        public AmazonDynamoDBClient GetClient()
        {
            var credentials = new BasicAWSCredentials(_settings.AccessKey, _settings.Secret);
            var config = new AmazonDynamoDBConfig();
            config.RegionEndpoint = RegionEndpoint.EUWest1;

            return new AmazonDynamoDBClient(credentials, config);
        }

        public DynamoDBContext GetContext()
        {
            return new DynamoDBContext(GetClient());
        }

        public bool IsTableActive(string tableName)
        {
            var tableDescription = DescribeTableAsync(tableName).Result;

            if (tableDescription.HttpStatusCode == HttpStatusCode.OK && tableDescription.Table != null)
            {
                return tableDescription.Table.TableStatus == "ACTIVE";
            }

            return false;
        }

        public async Task<T> LoadAsync<T>(string service)
        {
            return await GetContext().LoadAsync<T>(typeof(T).Name);
        }

        public async Task SaveAsync<T>(object model)
        {
            await GetContext().SaveAsync((T)model);
        }
    }
}
