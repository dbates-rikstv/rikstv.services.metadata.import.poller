﻿using System.Threading.Tasks;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence.Models;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence
{
    public interface IPersistence
    {
        Task<PersistenceResponse> Setup();
        Task<T> Load<T>();
        Task<PersistenceResponse> Save(PercistenceModel model);

    }
}
