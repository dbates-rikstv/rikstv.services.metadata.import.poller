﻿namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence
{
    public enum PersistenceResult
    {
        Successful = 0,
        NotReady = 1,
        NotImplemented = 2,
        Failed = 3
    }

    public class PersistenceResponse
    {
        public bool Successful => Result == PersistenceResult.Successful;

        public PersistenceResult Result { get; private set; }
        public string ErrorMessage { get; private set; }

        public static PersistenceResponse Success()
        {
            return new PersistenceResponse
            {
                Result = PersistenceResult.Successful
            };
        }

        public static PersistenceResponse Failure(PersistenceResult result, string message)
        {
            return new PersistenceResponse
            {
                Result = result,
                ErrorMessage = message
            };
        }
    }
}
