﻿using Amazon.DynamoDBv2.DataModel;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence.Models
{
    public class PercistenceModel
    {
        [DynamoDBHashKey] //Partition key
        public string Service
        {
            get; set;
        }
    }
}
