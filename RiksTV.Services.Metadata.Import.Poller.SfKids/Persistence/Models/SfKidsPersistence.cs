﻿using Amazon.DynamoDBv2.DataModel;
using System;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence.Models
{
    [DynamoDBTable("RiksTv.Services.Metadata.Import.Poller")]
    public class SfKidsPersistence : PercistenceModel
    {
        public SfKidsPersistence()
        {
            Service = GetType().Name;
        }

        [DynamoDBProperty]
        public DateTime ModifiedSince
        {
            get; set;
        }
    }
}
