﻿using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence
{
    public interface IDynamoDb
    {
        AmazonDynamoDBClient GetClient();
        DynamoDBContext GetContext();

        Task<DescribeTableResponse> DescribeTableAsync(string table);

        Task<CreateTableResponse> CreateTableAsync(CreateTableRequest request);

        bool IsTableActive(string tableName);

        Task<T> LoadAsync<T>(string service);
        Task SaveAsync<T>(object model);

    }
}
