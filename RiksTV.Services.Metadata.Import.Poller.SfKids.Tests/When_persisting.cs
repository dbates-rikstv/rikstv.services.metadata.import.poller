﻿using System;
using System.Net;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.Model;
using FakeItEasy;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Persistence.Models;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using Xunit;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Tests
{
    public class When_persisting
    {
        private static string _tableName = "RiksTv.Services.Metadata.Import.Poller";

        [Fact]
        public async Task Given_blank_db_Should_create_table()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).Throws<Exception>().Once();
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).Returns(true);

            A.CallTo(() => dynamodb.CreateTableAsync(A<CreateTableRequest>._)).Returns(new CreateTableResponse()
                {
                    HttpStatusCode = HttpStatusCode.OK,
                    TableDescription = new TableDescription()
                }
            );

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Setup();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).MustHaveHappened();
            A.CallTo(() => dynamodb.CreateTableAsync(A<CreateTableRequest>._)).MustHaveHappened();
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).MustHaveHappenedOnceExactly();
            Assert.True(response.Successful);
            Assert.Null(response.ErrorMessage);
        }

        [Fact]
        public async Task Given_blank_db_create_failed_Should_fail()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).Throws<Exception>().Once();

            A.CallTo(() => dynamodb.CreateTableAsync(A<CreateTableRequest>._)).Returns(new CreateTableResponse()
                {
                    HttpStatusCode = HttpStatusCode.BadRequest
                }
            );

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Setup();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).MustHaveHappened();
            A.CallTo(() => dynamodb.CreateTableAsync(A<CreateTableRequest>._)).MustHaveHappened();
            Assert.False(response.Successful);
            Assert.Equal(PersistenceResult.Failed, response.Result);
            Assert.Equal("Can't create percistency table.", response.ErrorMessage);
        }

        [Fact]
        public async Task Given_blank_db_create_timeout_Should_return_not_ready()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 2
            });

            var dynamodb = A.Fake<IDynamoDb>();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).Throws<Exception>().Once();
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).Returns(false);
            A.CallTo(() => dynamodb.CreateTableAsync(A<CreateTableRequest>._)).Returns(new CreateTableResponse()
                {
                    HttpStatusCode = HttpStatusCode.OK,
                    TableDescription = new TableDescription()
                }
            );

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Setup();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).MustHaveHappened();
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).MustHaveHappenedTwiceExactly();
            A.CallTo(() => dynamodb.CreateTableAsync(A<CreateTableRequest>._)).MustHaveHappened();
            Assert.False(response.Successful);
            Assert.Equal(PersistenceResult.NotReady, response.Result);
            Assert.Equal("DynamoDB Table not ready.", response.ErrorMessage);
        }

        [Fact]
        public async Task Given_existing_table_Should_return_successful()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).Returns(new DescribeTableResponse()
                {
                    HttpStatusCode = HttpStatusCode.OK
                }
            );
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).Returns(true);

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Setup();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).MustHaveHappened();
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).MustHaveHappenedOnceExactly();
            Assert.True(response.Successful);
            Assert.Null(response.ErrorMessage);
        }

        [Fact]
        public async Task Given_existing_non_active_table_Should_return_not_ready()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).Returns(new DescribeTableResponse()
                {
                    HttpStatusCode = HttpStatusCode.OK
                }
            );
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).Returns(false);

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Setup();

            A.CallTo(() => dynamodb.DescribeTableAsync(_tableName)).MustHaveHappened();
            A.CallTo(() => dynamodb.IsTableActive(_tableName)).MustHaveHappenedOnceExactly();
            Assert.False(response.Successful);
            Assert.Equal(PersistenceResult.NotReady, response.Result);
            Assert.Equal("DynamoDB Table not ready.", response.ErrorMessage);
        }

        [Fact]
        public async Task Given_valid_sfkids_object_Should_save_succesfully()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();

            A.CallTo(() => dynamodb.SaveAsync<SfKidsPersistence>(A<SfKidsPersistence>._)).Returns(Task.FromResult(PersistenceResponse.Success()));

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Save(new SfKidsPersistence()
            {
                ModifiedSince = DateTime.Now
            });

            A.CallTo(() => dynamodb.SaveAsync<SfKidsPersistence>(A<SfKidsPersistence>._)).MustHaveHappened();
            Assert.True(response.Successful);
            Assert.Null(response.ErrorMessage);
        }

        [Fact]
        public async Task Given_unsupported_object_Should_not_save()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();
            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);

            var response = await persistence.Save(new PercistenceModel());
            A.CallTo(() => dynamodb.SaveAsync<SfKidsPersistence>(A<SfKidsPersistence>._)).MustNotHaveHappened();
            Assert.False(response.Successful);
            Assert.Equal(PersistenceResult.NotImplemented, response.Result);
            Assert.Equal("Storage of model type not implemented.", response.ErrorMessage);
        }

        [Fact]
        public async Task Given_null_object_Should_not_save()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();
            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);

            var response = await persistence.Save(null);
            A.CallTo(() => dynamodb.SaveAsync<SfKidsPersistence>(A<SfKidsPersistence>._)).MustNotHaveHappened();
            Assert.False(response.Successful);
            Assert.Equal(PersistenceResult.NotImplemented, response.Result);
            Assert.Equal("Storage of model type not implemented.", response.ErrorMessage);
        }

        [Fact]
        public async Task Load_sfkids_object_Should_succeed()
        {
            var settings = A.Fake<IOptions<DynamoDbSettings>>();
            A.CallTo(() => settings.Value).Returns(new DynamoDbSettings
            {
                AccessKey = "Fake",
                Secret = "Fake",
                Table = "Fake",
                CreateTableTimeout = 5
            });

            var dynamodb = A.Fake<IDynamoDb>();


            A.CallTo(() => dynamodb.LoadAsync<SfKidsPersistence>(typeof(SfKidsPersistence).Name)).Returns(Task.FromResult(new SfKidsPersistence() { ModifiedSince = DateTime.Now }));

            var persistence = new DynamoDbPersistence(dynamodb, settings, NullLogger<App>.Instance);
            var response = await persistence.Load<SfKidsPersistence>();

            A.CallTo(() => dynamodb.LoadAsync<SfKidsPersistence>(typeof(SfKidsPersistence).Name)).MustHaveHappened();
            Assert.IsType<SfKidsPersistence>(response);
            Assert.Equal(typeof(SfKidsPersistence).Name, response.Service);
        }
    }
}