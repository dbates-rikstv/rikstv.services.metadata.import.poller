﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Tests
{
    class MockRiksTvApiHandler : HttpMessageHandler
    {
        readonly private HttpStatusCode _statusCode;

        public MockRiksTvApiHandler(HttpStatusCode statusCode)
        {
            _statusCode = statusCode;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();

            tcs.SetResult(new HttpResponseMessage(_statusCode));

            return tcs.Task;
        }
    }
}
