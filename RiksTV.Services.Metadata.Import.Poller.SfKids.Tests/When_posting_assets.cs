﻿using System.Net;
using FakeItEasy;
using Microsoft.Extensions.Options;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Services;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json.Linq;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Tests
{
    public class When_posting_assets
    {
        [Fact]
        public async Task Given_asset_Should_succeed()
        {
            var options = A.Fake<IOptions<RiksTvApiSettings>>();
            A.CallTo(() => options.Value).Returns(new RiksTvApiSettings
            {
                EndpointUrl = "http://rikstv.mock.api"
            });

            var httpClient = new HttpClient(new MockRiksTvApiHandler(HttpStatusCode.OK));
            var poster = new RiksTvPoster(options, httpClient, NullLogger<App>.Instance);


            var ret = await poster.PostAsset(new JObject());

            Assert.NotNull(ret);
            Assert.IsType<RiksTvApiResponse>(ret);
            Assert.True(ret.Successful);
            Assert.Null(ret.ErrorMessage);
        }

        [Fact]
        public async Task Given_null_asset_Should_fail()
        {
            var options = A.Fake<IOptions<RiksTvApiSettings>>();
            A.CallTo(() => options.Value).Returns(new RiksTvApiSettings
            {
                EndpointUrl = "http://rikstv.mock.api"
            });

            var httpClient = new HttpClient(new MockRiksTvApiHandler(HttpStatusCode.OK));
            var poster = new RiksTvPoster(options, httpClient, NullLogger<App>.Instance);


            var ret = await poster.PostAsset(null);

            Assert.NotNull(ret);
            Assert.IsType<RiksTvApiResponse>(ret);
            Assert.False(ret.Successful);
            Assert.Equal(RiksTvApiResult.NullData, ret.Result);
            Assert.Equal("Can't post null data object.", ret.ErrorMessage);
        }

        [Fact]
        public async Task Given_asset_with_bad_data_Should_fail()
        {
            var options = A.Fake<IOptions<RiksTvApiSettings>>();
            A.CallTo(() => options.Value).Returns(new RiksTvApiSettings
            {
                EndpointUrl = "http://rikstv.mock.api"
            });

            var httpClient = new HttpClient(new MockRiksTvApiHandler(HttpStatusCode.BadRequest));
            var poster = new RiksTvPoster(options, httpClient, NullLogger<App>.Instance);

            var ret = await poster.PostAsset(new JObject());

            Assert.NotNull(ret);
            Assert.IsType<RiksTvApiResponse>(ret);
            Assert.False(ret.Successful);
            Assert.Equal(RiksTvApiResult.BadRequest, ret.Result);
            Assert.Equal("Provided data not accepted.", ret.ErrorMessage);
        }

        [Fact]
        public async Task Given_asset_from_forbidden_ip_Should_fail()
        {
            var options = A.Fake<IOptions<RiksTvApiSettings>>();
            A.CallTo(() => options.Value).Returns(new RiksTvApiSettings
            {
                EndpointUrl = "http://rikstv.mock.api"
            });

            var httpClient = new HttpClient(new MockRiksTvApiHandler(HttpStatusCode.Forbidden));
            var poster = new RiksTvPoster(options, httpClient, NullLogger<App>.Instance);

            var ret = await poster.PostAsset(new JObject());

            Assert.NotNull(ret);
            Assert.IsType<RiksTvApiResponse>(ret);
            Assert.False(ret.Successful);
            Assert.Equal(RiksTvApiResult.AccessDenied, ret.Result);
            Assert.Equal("Call made from illegal IP address.", ret.ErrorMessage);
        }

    }
}
