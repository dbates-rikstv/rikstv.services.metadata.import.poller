﻿using System.Net;
using System.Net.Http;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Services;
using System.Threading.Tasks;
using RiksTV.Services.Metadata.Import.Poller.SfKids.Settings;
using Xunit;
using Microsoft.Extensions.Options;
using FakeItEasy;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json.Linq;

namespace RiksTV.Services.Metadata.Import.Poller.SfKids.Tests
{
    public class When_requesting_asset
    {
        [Fact]
        public async Task Given_valid_episode_page_url_Should_return_asset()
        {
            var options = A.Fake<IOptions<PartnerApiSettings>>();
            A.CallTo(() => options.Value).Returns(new PartnerApiSettings
            {
                EpisodesBaseUrl = "http://mock.partner.api/Episodes",
                MoviesBaseUrl = "http://mock.partner.api/Movies"
            });

            var request = new HttpRequestMessage(HttpMethod.Post, "http://mock.partner.api/Episodes");
            var httpClient = new HttpClient(new MockSfHttpMessageHandler());

            var fetcher = new SfFetcher(options, httpClient, NullLogger<App>.Instance);
            var ret = await fetcher.GetNextChangedAsset();

            Assert.NotNull(ret);
            Assert.IsType<JObject>(ret);
            Assert.Equal("Series", ret["type"]);
            Assert.NotNull(ret["item"]);
            Assert.Equal(52, ret["episodecount"]);
            Assert.NotNull(ret["season"]);
            Assert.NotNull(ret["series"]);
        }

        [Fact]
        public async Task Given_last_episode_Should_iterate_to_movie_asset()
        {
            var options = A.Fake<IOptions<PartnerApiSettings>>();
            A.CallTo(() => options.Value).Returns(new PartnerApiSettings
            {
                EpisodesBaseUrl = "http://mock.partner.api/Episodes",
                MoviesBaseUrl = "http://mock.partner.api/Movies"
            });

            var request = new HttpRequestMessage(HttpMethod.Post, "http://mock.partner.api/Episodes");
            var httpClient = new HttpClient(new MockSfHttpMessageHandler());

            var fetcher = new SfFetcher(options, httpClient, NullLogger<App>.Instance);
            var ret = await fetcher.GetNextChangedAsset();

            ret = await fetcher.GetNextChangedAsset();

            Assert.NotNull(ret);
            Assert.IsType<JObject>(ret);
            Assert.Equal("Movie", ret["type"]);
            Assert.NotNull(ret["item"]);
        }

        [Fact]
        public async Task Given_last_movie_Should_return_null_asset()
        {
            var options = A.Fake<IOptions<PartnerApiSettings>>();
            A.CallTo(() => options.Value).Returns(new PartnerApiSettings
            {
                EpisodesBaseUrl = "http://mock.partner.api/Episodes",
                MoviesBaseUrl = "http://mock.partner.api/Movies"
            });

            var request = new HttpRequestMessage(HttpMethod.Post, "http://mock.partner.api/Episodes");
            var httpClient = new HttpClient(new MockSfHttpMessageHandler());

            var fetcher = new SfFetcher(options, httpClient, NullLogger<App>.Instance);

            var ret = await fetcher.GetNextChangedAsset();
            ret = await fetcher.GetNextChangedAsset();
            ret = await fetcher.GetNextChangedAsset();

            Assert.Null(ret);
        }
    }
}
